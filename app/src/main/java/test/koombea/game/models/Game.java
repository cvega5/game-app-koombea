package test.koombea.game.models;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by USER on 24/06/2019.
 */

public class Game extends RealmObject{

    private String objectId, name, universe, imageUrl, kind, description, sku;
    private Double rating, price, downloads;
    private Boolean popular;
    private Date createdAt;
    private Date updatedAt;

    public Game() {
    }

    public Game(String objectId, String name, String universe, String imageUrl, String kind, Double downloads, String description, String sku, Double price, Double rating, Boolean popular, Date createdAt, Date updatedAt) {
        this.objectId = objectId;
        this.name = name;
        this.universe = universe;
        this.imageUrl = imageUrl;
        this.kind = kind;
        this.downloads = downloads;
        this.description = description;
        this.sku = sku;
        this.price = price;
        this.rating = rating;
        this.popular = popular;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniverse() {
        return universe;
    }

    public void setUniverse(String universe) {
        this.universe = universe;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Double getDownloads() {
        return downloads;
    }

    public void setDownloads(Double downloads) {
        this.downloads = downloads;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Boolean getPopular() {
        return popular;
    }

    public void setPopular(Boolean popular) {
        this.popular = popular;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
