package test.koombea.game.controllers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import test.koombea.game.R;
import test.koombea.game.adapters.newGamesAdapter;
import test.koombea.game.adapters.popularGamesAdapter;
import test.koombea.game.adapters.universosAdapter;
import test.koombea.game.helpers.Funciones;
import test.koombea.game.helpers.MyVolleySingleton;
import test.koombea.game.helpers.controller_consultas;
import test.koombea.game.models.Game;

public class MainActivity extends AppCompatActivity implements universosAdapter.ViewHolder.onUniversosClicklistener,
        newGamesAdapter.ViewHolder.onGameClicklistener, popularGamesAdapter.ViewHolder.onGameClicklistener {

    private RequestQueue queue;
    public static final String TAG = "Main";
    private RecyclerView universes, newGames, popularGames;
    private RecyclerView.Adapter adapter_universes, adapter_new_games, adapter_popular_games;
    private TextView txtnew, txtpopular, txtall, txt_line;
    private ArrayList<Game> games;
    private LinearLayout linearRight, linearLeft;
    private RealmConfiguration realmConfiguration;
    private ImageButton filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Realm
        Realm.init(MainActivity.this);

        realmConfiguration = new RealmConfiguration.Builder()
                .name("realm_game_BD.realm")
                .schemaVersion(2)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        queue = MyVolleySingleton.getInstance(this.getApplicationContext()).getRequestQueue();

        universes = findViewById(R.id.list_universes);
        newGames = findViewById(R.id.newGames);
        popularGames = findViewById(R.id.popularGames);
        txtnew = findViewById(R.id.txtnuevos);
        txtpopular = findViewById(R.id.txtPopulares);
        txtall = findViewById(R.id.txtAll);
        linearRight = findViewById(R.id.linear_derecha);
        linearLeft = findViewById(R.id.linear_izquierda);
        txt_line = findViewById(R.id.textView4);
        filter = findViewById(R.id.imageButton);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager mLayoutManager_2 = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager mLayoutManager_3 = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);

        universes.setLayoutManager(mLayoutManager);
        newGames.setLayoutManager(mLayoutManager_2);
        popularGames.setLayoutManager(mLayoutManager_3);

        //adapters
        adapter_universes = new universosAdapter(this, MainActivity.this);
        adapter_new_games = new newGamesAdapter(this, MainActivity.this, txtnew);
        adapter_popular_games = new popularGamesAdapter(this, MainActivity.this, txtpopular);

        universes.setAdapter(adapter_universes);
        newGames.setAdapter(adapter_new_games);
        popularGames.setAdapter(adapter_popular_games);

        if (Funciones.isNetAvailable(MainActivity.this.getApplicationContext())) {
            getGames();
        } else {
            getGamesOffline();
        }

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FilterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }

    @Override
    public void onUniversosClick(Game mGame) {
        if(mGame.getUniverse().equals("All")){
            txt_line.setVisibility(View.VISIBLE);
            txtpopular.setVisibility(View.VISIBLE);
            popularGames.setVisibility(View.VISIBLE);

            adapter_new_games = new newGamesAdapter(this, MainActivity.this, txtnew);
            newGames.setAdapter(adapter_new_games);
            if (Funciones.isNetAvailable(MainActivity.this.getApplicationContext())) {
                getGames();
            } else {
                getGamesOffline();
            }
        } else {
            txt_line.setVisibility(View.GONE);
            txtpopular.setVisibility(View.GONE);
            popularGames.setVisibility(View.GONE);

            adapter_new_games = new newGamesAdapter(this, MainActivity.this, txtnew, mGame.getUniverse());
            newGames.setAdapter(adapter_new_games);
            if (Funciones.isNetAvailable(MainActivity.this.getApplicationContext())) {
                getGames(mGame.getUniverse());
                linearLeft.removeAllViews();
                linearRight.removeAllViews();
            } else {
                getGamesOffline(mGame.getUniverse());
            }
        }
    }

    @Override
    public void onNewGamesClick(Game mGame) {
        Intent intent = new Intent(MainActivity.this, DetailGame.class);
        Bundle bundle = new Bundle();
        bundle.putString("sku", mGame.getSku());
        bundle.putString("nombre", mGame.getName());
        bundle.putString("universe", mGame.getUniverse());
        bundle.putString("kind", mGame.getKind());
        bundle.putDouble("downloads", mGame.getDownloads());
        bundle.putDouble("rating", mGame.getRating());
        bundle.putDouble("price", mGame.getPrice());
        bundle.putString("description", mGame.getDescription());
        bundle.putString("imageurl", mGame.getImageUrl());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onPopularGamesClick(Game mGame) {
        Intent intent = new Intent(MainActivity.this, DetailGame.class);
        Bundle bundle = new Bundle();
        bundle.putString("sku", mGame.getSku());
        bundle.putString("nombre", mGame.getName());
        bundle.putString("universe", mGame.getUniverse());
        bundle.putString("kind", mGame.getKind());
        bundle.putDouble("downloads", mGame.getDownloads());
        bundle.putDouble("rating", mGame.getRating());
        bundle.putDouble("price", mGame.getPrice());
        bundle.putString("description", mGame.getDescription());
        bundle.putString("imageurl", mGame.getImageUrl());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void getGames() {
        String url = MainActivity.this.getString(R.string.url)+"classes/Product";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        linearLeft.removeAllViews();
                        linearRight.removeAllViews();

                        games = parseJson(response);
                        if (games.size() > 0) {
                            txtall.setText("All ("+(games.size())+")");

                            Realm realm = Realm.getDefaultInstance();
                            controller_consultas consultas = new controller_consultas(realm);
                            consultas.eliminarJuegos();
                            consultas.guardarJuegos(games);
                            realm.close();
                        }
                        Collections.sort(games, new Comparator<Game>() {
                            @Override
                            public int compare(Game game, Game t1) {
                                if(game.getName() == null || t1.getName() == null)
                                    return 0;
                                return game.getName().compareTo(t1.getName());
                            }
                        });

                        for (int i = 0; i < games.size(); i++) {
                            if (i % 2 == 0){
                                final int position = i;
                                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                                nombre.setText(games.get(i).getName());
                                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                                Glide.with(MainActivity.this.getApplicationContext())
                                        .load(games.get(i).getImageUrl())
                                        .apply(requestOptions)
                                        .into(imagen);

                                item.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(MainActivity.this, DetailGame.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("sku", games.get(position).getSku());
                                        bundle.putString("nombre", games.get(position).getName());
                                        bundle.putString("universe", games.get(position).getUniverse());
                                        bundle.putString("kind", games.get(position).getKind());
                                        bundle.putDouble("downloads", games.get(position).getDownloads());
                                        bundle.putDouble("rating", games.get(position).getRating());
                                        bundle.putDouble("price", games.get(position).getPrice());
                                        bundle.putString("description", games.get(position).getDescription());
                                        bundle.putString("imageurl", games.get(position).getImageUrl());
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                });

                                linearLeft.addView(item);

                            } else {
                                final int position = i;
                                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                                nombre.setText(games.get(i).getName());
                                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                                Glide.with(MainActivity.this.getApplicationContext())
                                        .load(games.get(i).getImageUrl())
                                        .apply(requestOptions)
                                        .into(imagen);

                                item.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(MainActivity.this, DetailGame.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("sku", games.get(position).getSku());
                                        bundle.putString("nombre", games.get(position).getName());
                                        bundle.putString("universe", games.get(position).getUniverse());
                                        bundle.putString("kind", games.get(position).getKind());
                                        bundle.putDouble("downloads", games.get(position).getDownloads());
                                        bundle.putDouble("rating", games.get(position).getRating());
                                        bundle.putDouble("price", games.get(position).getPrice());
                                        bundle.putString("description", games.get(position).getDescription());
                                        bundle.putString("imageurl", games.get(position).getImageUrl());
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                });

                                linearRight.addView(item);
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error_getUniverses", error.getMessage());
                    }
                })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("X-Parse-Application-Id", MainActivity.this.getString(R.string.parse_app_id));
                headers.put("X-Parse-REST-API-Key", MainActivity.this.getString(R.string.rest_api_key));
                return headers;
            }
        };
        request.setTag(TAG);
        MyVolleySingleton.getInstance(MainActivity.this.getApplicationContext()).addToRequestQueue(request);
    }

    private void getGames(final String universe) {
        String url = MainActivity.this.getString(R.string.url)+"classes/Product";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        games = parseJson(response, universe);
                        if (games.size() > 0) {
                            txtall.setText("All ("+(games.size())+")");
                        }
                        Collections.sort(games, new Comparator<Game>() {
                            @Override
                            public int compare(Game game, Game t1) {
                                if(game.getName() == null || t1.getName() == null)
                                    return 0;
                                return game.getName().compareTo(t1.getName());
                            }
                        });

                        for (int i = 0; i < games.size(); i++) {
                            if (i % 2 == 0){
                                final int position = i;
                                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                                nombre.setText(games.get(i).getName());
                                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                                Glide.with(MainActivity.this.getApplicationContext())
                                        .load(games.get(i).getImageUrl())
                                        .apply(requestOptions)
                                        .into(imagen);

                                item.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(MainActivity.this, DetailGame.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("sku", games.get(position).getSku());
                                        bundle.putString("nombre", games.get(position).getName());
                                        bundle.putString("universe", games.get(position).getUniverse());
                                        bundle.putString("kind", games.get(position).getKind());
                                        bundle.putDouble("downloads", games.get(position).getDownloads());
                                        bundle.putDouble("rating", games.get(position).getRating());
                                        bundle.putDouble("price", games.get(position).getPrice());
                                        bundle.putString("description", games.get(position).getDescription());
                                        bundle.putString("imageurl", games.get(position).getImageUrl());
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                });

                                linearLeft.addView(item);

                            } else {
                                final int position = i;
                                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                                nombre.setText(games.get(i).getName());
                                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                                Glide.with(MainActivity.this.getApplicationContext())
                                        .load(games.get(i).getImageUrl())
                                        .apply(requestOptions)
                                        .into(imagen);

                                item.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(MainActivity.this, DetailGame.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("sku", games.get(position).getSku());
                                        bundle.putString("nombre", games.get(position).getName());
                                        bundle.putString("universe", games.get(position).getUniverse());
                                        bundle.putString("kind", games.get(position).getKind());
                                        bundle.putDouble("downloads", games.get(position).getDownloads());
                                        bundle.putDouble("rating", games.get(position).getRating());
                                        bundle.putDouble("price", games.get(position).getPrice());
                                        bundle.putString("description", games.get(position).getDescription());
                                        bundle.putString("imageurl", games.get(position).getImageUrl());
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                });

                                linearRight.addView(item);
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error_getUniverses", error.getMessage());
                    }
                })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("X-Parse-Application-Id", MainActivity.this.getString(R.string.parse_app_id));
                headers.put("X-Parse-REST-API-Key", MainActivity.this.getString(R.string.rest_api_key));
                return headers;
            }
        };
        request.setTag(TAG);
        MyVolleySingleton.getInstance(MainActivity.this.getApplicationContext()).addToRequestQueue(request);
    }

    public ArrayList<Game> parseJson(JSONObject jsonObject) {
        ArrayList<Game> games = new ArrayList<>();
        JSONArray jsonArray = null;

        try {
            jsonArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);

                Date created = null, updated = null;
                String createdDate = object.getString("createdAt");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                try {
                    created = format.parse(createdDate);
                    System.out.println(created);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String updatedDate = object.getString("updatedAt");
                try {
                    updated = format.parse(updatedDate);
                    System.out.println(updated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Double downloads = Double.valueOf(object.getString("downloads"));
                Double price = Double.valueOf(object.getString("price").replace(",", "."));

                Game movie = new Game(
                        object.getString("objectId"),
                        object.getString("name"),
                        object.getString("universe"),
                        object.getString("imageURL"),
                        object.getString("kind"),
                        downloads,
                        object.getString("description"),
                        object.getString("SKU"),
                        price,
                        object.getDouble("rating"),
                        object.getBoolean("popular"),
                        created,
                        updated
                );

                games.add(movie);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return games;
    }

    public ArrayList<Game> parseJson(JSONObject jsonObject, String universe) {
        ArrayList<Game> games = new ArrayList<>();
        JSONArray jsonArray = null;

        try {
            jsonArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);

                Date created = null, updated = null;
                String createdDate = object.getString("createdAt");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                try {
                    created = format.parse(createdDate);
                    System.out.println(created);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String updatedDate = object.getString("updatedAt");
                try {
                    updated = format.parse(updatedDate);
                    System.out.println(updated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Double downloads = Double.valueOf(object.getString("downloads"));
                Double price = Double.valueOf(object.getString("price").replace(",", "."));

                if (object.getString("universe").equals(universe)) {
                    Game movie = new Game(
                            object.getString("objectId"),
                            object.getString("name"),
                            object.getString("universe"),
                            object.getString("imageURL"),
                            object.getString("kind"),
                            downloads,
                            object.getString("description"),
                            object.getString("SKU"),
                            price,
                            object.getDouble("rating"),
                            object.getBoolean("popular"),
                            created,
                            updated
                    );

                    games.add(movie);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return games;
    }

    public void getGamesOffline() {
        Realm realm = Realm.getDefaultInstance();
        controller_consultas consultas = new controller_consultas(realm);
        games = consultas.getJuegos();
        realm.close();

        linearLeft.removeAllViews();
        linearRight.removeAllViews();

        if (games.size() > 0) {
            txtall.setText("All ("+(games.size())+")");
        }

        Collections.sort(games, new Comparator<Game>() {
            @Override
            public int compare(Game game, Game t1) {
                if(game.getName() == null || t1.getName() == null)
                    return 0;
                return game.getName().compareTo(t1.getName());
            }
        });

        for (int i = 0; i < games.size(); i++) {
            if (i % 2 == 0){
                final int position = i;
                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                nombre.setText(games.get(i).getName());
                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                Glide.with(MainActivity.this.getApplicationContext())
                        .load(games.get(i).getImageUrl())
                        .apply(requestOptions)
                        .into(imagen);

                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, DetailGame.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("sku", games.get(position).getSku());
                        bundle.putString("nombre", games.get(position).getName());
                        bundle.putString("universe", games.get(position).getUniverse());
                        bundle.putString("kind", games.get(position).getKind());
                        bundle.putDouble("downloads", games.get(position).getDownloads());
                        bundle.putDouble("rating", games.get(position).getRating());
                        bundle.putDouble("price", games.get(position).getPrice());
                        bundle.putString("description", games.get(position).getDescription());
                        bundle.putString("imageurl", games.get(position).getImageUrl());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });

                linearLeft.addView(item);

            } else {
                final int position = i;
                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                nombre.setText(games.get(i).getName());
                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                Glide.with(MainActivity.this.getApplicationContext())
                        .load(games.get(i).getImageUrl())
                        .apply(requestOptions)
                        .into(imagen);

                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, DetailGame.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("sku", games.get(position).getSku());
                        bundle.putString("nombre", games.get(position).getName());
                        bundle.putString("universe", games.get(position).getUniverse());
                        bundle.putString("kind", games.get(position).getKind());
                        bundle.putDouble("downloads", games.get(position).getDownloads());
                        bundle.putDouble("rating", games.get(position).getRating());
                        bundle.putDouble("price", games.get(position).getPrice());
                        bundle.putString("description", games.get(position).getDescription());
                        bundle.putString("imageurl", games.get(position).getImageUrl());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });

                linearRight.addView(item);
            }
        }

    }

    public void getGamesOffline(final String universe) {

        Realm realm = Realm.getDefaultInstance();
        controller_consultas consultas = new controller_consultas(realm);
        ArrayList<Game> lista = consultas.getJuegos();
        realm.close();

        linearLeft.removeAllViews();
        linearRight.removeAllViews();
        games.clear();

        for (int i = 0; i < lista.size(); i++) {
            if(lista.get(i).getUniverse().equals(universe)){
                games.add(lista.get(i));
            }
        }

        if (games.size() > 0) {
            txtall.setText("All ("+(games.size())+")");
        }

        Collections.sort(games, new Comparator<Game>() {
            @Override
            public int compare(Game game, Game t1) {
                if(game.getName() == null || t1.getName() == null)
                    return 0;
                return game.getName().compareTo(t1.getName());
            }
        });

        for (int i = 0; i < games.size(); i++) {
            if (i % 2 == 0){
                final int position = i;
                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                nombre.setText(games.get(i).getName());
                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                Glide.with(MainActivity.this.getApplicationContext())
                        .load(games.get(i).getImageUrl())
                        .apply(requestOptions)
                        .into(imagen);

                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, DetailGame.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("sku", games.get(position).getSku());
                        bundle.putString("nombre", games.get(position).getName());
                        bundle.putString("universe", games.get(position).getUniverse());
                        bundle.putString("kind", games.get(position).getKind());
                        bundle.putDouble("downloads", games.get(position).getDownloads());
                        bundle.putDouble("rating", games.get(position).getRating());
                        bundle.putDouble("price", games.get(position).getPrice());
                        bundle.putString("description", games.get(position).getDescription());
                        bundle.putString("imageurl", games.get(position).getImageUrl());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });

                linearLeft.addView(item);

            } else {
                final int position = i;
                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                nombre.setText(games.get(i).getName());
                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                Glide.with(MainActivity.this.getApplicationContext())
                        .load(games.get(i).getImageUrl())
                        .apply(requestOptions)
                        .into(imagen);

                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, DetailGame.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("sku", games.get(position).getSku());
                        bundle.putString("nombre", games.get(position).getName());
                        bundle.putString("universe", games.get(position).getUniverse());
                        bundle.putString("kind", games.get(position).getKind());
                        bundle.putDouble("downloads", games.get(position).getDownloads());
                        bundle.putDouble("rating", games.get(position).getRating());
                        bundle.putDouble("price", games.get(position).getPrice());
                        bundle.putString("description", games.get(position).getDescription());
                        bundle.putString("imageurl", games.get(position).getImageUrl());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });

                linearRight.addView(item);
            }
        }

    }


}
