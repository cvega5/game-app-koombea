package test.koombea.game.controllers;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.text.NumberFormat;
import java.util.Locale;

import test.koombea.game.R;

public class DetailGame extends AppCompatActivity {

    private TextView txtsku, txtnombre, txtuniverse, txtkind, txtdownloads, txtprice, txtdescription;
    private RatingBar ratingBar;
    private ImageButton imageButton;
    private ImageView imagen;
    private LayerDrawable stars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_game);

        txtsku = findViewById(R.id.txt_sku);
        txtnombre = findViewById(R.id.txt_nombre);
        txtuniverse = findViewById(R.id.txt_universe);
        txtkind = findViewById(R.id.txt_kind);
        txtdownloads = findViewById(R.id.txt_downloads);
        txtprice = findViewById(R.id.txt_price);
        txtdescription = findViewById(R.id.txt_description);
        ratingBar = findViewById(R.id.rating_bar);
        imageButton = findViewById(R.id.imageButton2);
        imagen = findViewById(R.id.image_game);
        stars = (LayerDrawable) ratingBar.getProgressDrawable();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            txtsku.setText("SKU: ".concat(bundle.getString("sku")));
            txtnombre.setText(bundle.getString("nombre"));
            txtuniverse.setText(bundle.getString("universe"));
            txtkind.setText(bundle.getString("kind"));

            NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
            numberFormat.setMinimumFractionDigits(0);
            String numeroformateado2 = numberFormat.format(bundle.getDouble("downloads"));
            String numerofinal = String.valueOf(numeroformateado2);
            txtdownloads.setText(numerofinal.concat(" downloads"));

            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
            nf.setMinimumFractionDigits(2);
            String numeroformateado = nf.format(bundle.getDouble("price"));
            String numero = String.valueOf(numeroformateado);
            txtprice.setText(numero);

            txtdescription.setText(bundle.getString("description"));
            RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
            Glide.with(DetailGame.this.getApplicationContext())
                    .load(bundle.getString("imageurl"))
                    .apply(requestOptions)
                    .into(imagen);
            ratingBar.setRating(Float.parseFloat(String.valueOf(bundle.getDouble("rating"))));
            stars.getDrawable(2).setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        }

        txtdescription.setMovementMethod(new ScrollingMovementMethod());
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
