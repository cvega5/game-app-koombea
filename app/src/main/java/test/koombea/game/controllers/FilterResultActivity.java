package test.koombea.game.controllers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import test.koombea.game.R;
import test.koombea.game.helpers.Funciones;
import test.koombea.game.helpers.MyVolleySingleton;
import test.koombea.game.helpers.controller_consultas;
import test.koombea.game.models.Game;

public class FilterResultActivity extends AppCompatActivity {

    private LinearLayout linearRight, linearLeft;
    private TextView clear, txtFiltered;
    private ArrayList<Game> games;
    public static final String TAG = "Filter";
    private String filtro = "", filtroUniverse = "";
    private Double minimum, maximum;
    private double ratings[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_result);

        clear = findViewById(R.id.txt_limpiar_filtros);
        txtFiltered = findViewById(R.id.txt_filtered);
        linearLeft = findViewById(R.id.linear_izquierda_filter);
        linearRight = findViewById(R.id.linear_derecha_filter);

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("clear", true);
                setResult(FilterResultActivity.RESULT_OK, returnIntent);
                finish();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            filtro = bundle.getString("filtro");
            minimum = bundle.getDouble("minimum");
            maximum = bundle.getDouble("maximum");
            ratings = bundle.getDoubleArray("ratings");
            filtroUniverse = bundle.getString("filtroUniverso");
        }

        linearLeft.removeAllViews();
        linearRight.removeAllViews();

        if (Funciones.isNetAvailable(FilterResultActivity.this)) {
            getGames(filtro);
        } else {
            getGamesOffline(filtro);
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(FilterResultActivity.RESULT_CANCELED, returnIntent);
        finish();
    }

    private void getGames(final String filter) {
        String url = FilterResultActivity.this.getString(R.string.url)+"classes/Product";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        games = parseJson(response);

                        switch (filter) {
                            case "Downloads":
                                Collections.sort(games, new Comparator<Game>() {
                                    @Override
                                    public int compare(Game game, Game t1) {
                                        if(game.getDownloads() == null || t1.getDownloads() == null)
                                            return 0;
                                        return game.getDownloads().compareTo(t1.getDownloads());
                                    }
                                });
                                break;
                            case "Date added":
                                Collections.sort(games, new Comparator<Game>() {
                                    @Override
                                    public int compare(Game game, Game t1) {
                                        if(game.getCreatedAt() == null || t1.getCreatedAt() == null)
                                            return 0;
                                        return game.getCreatedAt().compareTo(t1.getCreatedAt());
                                    }
                                });
                                break;
                            case "Price":
                                Collections.sort(games, new Comparator<Game>() {
                                    @Override
                                    public int compare(Game game, Game t1) {
                                        return Double.compare(game.getPrice(),t1.getPrice());
                                    }
                                });
                                break;
                            default:
                                break;
                        }

                        boolean hayRating = false;
                        for (int i = 0; i < ratings.length; i++) {
                            if (ratings[i] > 0.0) {
                                hayRating = true;
                            }
                        }

                        ArrayList<Game> list = new ArrayList<>();
                        for (int i = 0; i < games.size(); i++) {
                            if (filtroUniverse.equals("All")) {
                                if(games.get(i).getPrice() >= minimum && games.get(i).getPrice() <= maximum){
                                    if (hayRating) {
                                        for (double rating : ratings) {
                                            if (rating == games.get(i).getRating()) {
                                                list.add(games.get(i));
                                            }
                                        }
                                    } else {
                                        list.add(games.get(i));
                                    }
                                }
                            } else {
                                if(games.get(i).getPrice() >= minimum && games.get(i).getPrice() <= maximum
                                        && games.get(i).getUniverse().equals(filtroUniverse)){
                                    if (hayRating) {
                                        for (double rating : ratings) {
                                            if (rating == games.get(i).getRating()) {
                                                list.add(games.get(i));
                                            }
                                        }
                                    } else {
                                        list.add(games.get(i));
                                    }
                                }
                            }
                        }

                        games = list;


                        if (games.size() > 0) {
                            txtFiltered.setText("Filtered ("+(games.size())+")");
                        } else {
                            txtFiltered.setText("Filtered (0)");
                        }

                        //region asignación de vistas
                        for (int i = 0; i < games.size(); i++) {
                            if (i % 2 == 0){
                                final int position = i;
                                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                                nombre.setText(games.get(i).getName());
                                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                                Glide.with(FilterResultActivity.this.getApplicationContext())
                                        .load(games.get(i).getImageUrl())
                                        .apply(requestOptions)
                                        .into(imagen);

                                item.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(FilterResultActivity.this, DetailGame.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("sku", games.get(position).getSku());
                                        bundle.putString("nombre", games.get(position).getName());
                                        bundle.putString("universe", games.get(position).getUniverse());
                                        bundle.putString("kind", games.get(position).getKind());
                                        bundle.putDouble("downloads", games.get(position).getDownloads());
                                        bundle.putDouble("rating", games.get(position).getRating());
                                        bundle.putDouble("price", games.get(position).getPrice());
                                        bundle.putString("description", games.get(position).getDescription());
                                        bundle.putString("imageurl", games.get(position).getImageUrl());
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                });

                                linearLeft.addView(item);

                            } else {
                                final int position = i;
                                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                                nombre.setText(games.get(i).getName());
                                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                                Glide.with(FilterResultActivity.this.getApplicationContext())
                                        .load(games.get(i).getImageUrl())
                                        .apply(requestOptions)
                                        .into(imagen);

                                item.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(FilterResultActivity.this, DetailGame.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("sku", games.get(position).getSku());
                                        bundle.putString("nombre", games.get(position).getName());
                                        bundle.putString("universe", games.get(position).getUniverse());
                                        bundle.putString("kind", games.get(position).getKind());
                                        bundle.putDouble("downloads", games.get(position).getDownloads());
                                        bundle.putDouble("rating", games.get(position).getRating());
                                        bundle.putDouble("price", games.get(position).getPrice());
                                        bundle.putString("description", games.get(position).getDescription());
                                        bundle.putString("imageurl", games.get(position).getImageUrl());
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                });

                                linearRight.addView(item);
                            }
                        }
                        //endregion
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error_getUniverses", error.getMessage());
                    }
                })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("X-Parse-Application-Id", FilterResultActivity.this.getString(R.string.parse_app_id));
                headers.put("X-Parse-REST-API-Key", FilterResultActivity.this.getString(R.string.rest_api_key));
                return headers;
            }
        };
        request.setTag(TAG);
        MyVolleySingleton.getInstance(FilterResultActivity.this.getApplicationContext()).addToRequestQueue(request);
    }

    private ArrayList<Game> parseJson(JSONObject jsonObject) {
        ArrayList<Game> games = new ArrayList<>();
        JSONArray jsonArray = null;

        try {
            jsonArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);

                Date created = null, updated = null;
                String createdDate = object.getString("createdAt");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                try {
                    created = format.parse(createdDate);
                    System.out.println(created);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String updatedDate = object.getString("updatedAt");
                try {
                    updated = format.parse(updatedDate);
                    System.out.println(updated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Double downloads = Double.valueOf(object.getString("downloads"));
                Double price = Double.valueOf(object.getString("price").replace(",", "."));

                Game movie = new Game(
                        object.getString("objectId"),
                        object.getString("name"),
                        object.getString("universe"),
                        object.getString("imageURL"),
                        object.getString("kind"),
                        downloads,
                        object.getString("description"),
                        object.getString("SKU"),
                        price,
                        object.getDouble("rating"),
                        object.getBoolean("popular"),
                        created,
                        updated
                );

                games.add(movie);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return games;
    }

    public void getGamesOffline (final String filter) {
        Realm realm = Realm.getDefaultInstance();
        controller_consultas consultas = new controller_consultas(realm);
        games = consultas.getJuegos();
        realm.close();

        switch (filter) {
            case "Downloads":
                Collections.sort(games, new Comparator<Game>() {
                    @Override
                    public int compare(Game game, Game t1) {
                        if(game.getDownloads() == null || t1.getDownloads() == null)
                            return 0;
                        return game.getDownloads().compareTo(t1.getDownloads());
                    }
                });
                break;
            case "Date added":
                Collections.sort(games, new Comparator<Game>() {
                    @Override
                    public int compare(Game game, Game t1) {
                        if(game.getCreatedAt() == null || t1.getCreatedAt() == null)
                            return 0;
                        return game.getCreatedAt().compareTo(t1.getCreatedAt());
                    }
                });
                break;
            case "Price":
                Collections.sort(games, new Comparator<Game>() {
                    @Override
                    public int compare(Game game, Game t1) {
                        return Double.compare(game.getPrice(),t1.getPrice());
                    }
                });
                break;
            default:
                break;
        }

        boolean hayRating = false;
        for (int i = 0; i < ratings.length; i++) {
            if (ratings[i] > 0.0) {
                hayRating = true;
            }
        }

        ArrayList<Game> list = new ArrayList<>();
        for (int i = 0; i < games.size(); i++) {
            if (filtroUniverse.equals("All")) {
                if(games.get(i).getPrice() >= minimum && games.get(i).getPrice() <= maximum){
                    if (hayRating) {
                        for (double rating : ratings) {
                            if (rating == games.get(i).getRating()) {
                                list.add(games.get(i));
                            }
                        }
                    } else {
                        list.add(games.get(i));
                    }
                }
            } else {
                if(games.get(i).getPrice() >= minimum && games.get(i).getPrice() <= maximum
                        && games.get(i).getUniverse().equals(filtroUniverse)){
                    if (hayRating) {
                        for (double rating : ratings) {
                            if (rating == games.get(i).getRating()) {
                                list.add(games.get(i));
                            }
                        }
                    } else {
                        list.add(games.get(i));
                    }
                }
            }
        }

        games = list;


        if (games.size() > 0) {
            txtFiltered.setText("Filtered ("+(games.size())+")");
        } else {
            txtFiltered.setText("Filtered (0)");
        }

        //region asignación de vistas
        for (int i = 0; i < games.size(); i++) {
            if (i % 2 == 0){
                final int position = i;
                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                nombre.setText(games.get(i).getName());
                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                Glide.with(FilterResultActivity.this.getApplicationContext())
                        .load(games.get(i).getImageUrl())
                        .apply(requestOptions)
                        .into(imagen);

                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(FilterResultActivity.this, DetailGame.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("sku", games.get(position).getSku());
                        bundle.putString("nombre", games.get(position).getName());
                        bundle.putString("universe", games.get(position).getUniverse());
                        bundle.putString("kind", games.get(position).getKind());
                        bundle.putDouble("downloads", games.get(position).getDownloads());
                        bundle.putDouble("rating", games.get(position).getRating());
                        bundle.putDouble("price", games.get(position).getPrice());
                        bundle.putString("description", games.get(position).getDescription());
                        bundle.putString("imageurl", games.get(position).getImageUrl());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });

                linearLeft.addView(item);

            } else {
                final int position = i;
                View item = getLayoutInflater().inflate(R.layout.item_juego_2, null);
                TextView nombre = item.findViewById(R.id.nombre_juego_2);
                ImageView imagen = item.findViewById(R.id.imagen_juego_2);

                nombre.setText(games.get(i).getName());
                RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                Glide.with(FilterResultActivity.this.getApplicationContext())
                        .load(games.get(i).getImageUrl())
                        .apply(requestOptions)
                        .into(imagen);

                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(FilterResultActivity.this, DetailGame.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("sku", games.get(position).getSku());
                        bundle.putString("nombre", games.get(position).getName());
                        bundle.putString("universe", games.get(position).getUniverse());
                        bundle.putString("kind", games.get(position).getKind());
                        bundle.putDouble("downloads", games.get(position).getDownloads());
                        bundle.putDouble("rating", games.get(position).getRating());
                        bundle.putDouble("price", games.get(position).getPrice());
                        bundle.putString("description", games.get(position).getDescription());
                        bundle.putString("imageurl", games.get(position).getImageUrl());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });

                linearRight.addView(item);
            }
        }
        //endregion
    }
}
