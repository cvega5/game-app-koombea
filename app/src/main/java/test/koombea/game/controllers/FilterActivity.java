package test.koombea.game.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.florescu.android.rangeseekbar.RangeSeekBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.realm.Realm;
import test.koombea.game.R;
import test.koombea.game.helpers.Funciones;
import test.koombea.game.helpers.MyVolleySingleton;
import test.koombea.game.helpers.controller_consultas;
import test.koombea.game.models.Game;

public class FilterActivity extends AppCompatActivity {

    private RadioGroup radioGroup, radioGroupUniverses;
    private RadioButton selected, selectedUniverse;
    private Button btnApply;
    private ArrayList<Game> games, universes;
    private RangeSeekBar<Double> rangeSeekBar;
    private Double minimumValue, maximumValue;
    private CheckBox stars_5, stars_4, stars_3, stars_2, stars_1;
    private double ratings[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Filters");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        radioGroup = findViewById(R.id.radio_group);
        btnApply = findViewById(R.id.button_apply);
        stars_1 = findViewById(R.id.checkbox_1_estrellas);
        stars_2 = findViewById(R.id.checkbox_2_estrellas);
        stars_3 = findViewById(R.id.checkbox_3_estrellas);
        stars_4 = findViewById(R.id.checkbox_4_estrellas);
        stars_5 = findViewById(R.id.checkbox_5_estrellas);
        ratings = new double[5];
        radioGroupUniverses = findViewById(R.id.radio_group_universes);


        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                selected = (RadioButton) findViewById(selectedId);
                int selectedUniverseId = radioGroupUniverses.getCheckedRadioButtonId();
                selectedUniverse = (RadioButton) findViewById(selectedUniverseId);

                Intent intent = new Intent(FilterActivity.this, FilterResultActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("filtro", selected.getText().toString());
                bundle.putDouble("minimum", minimumValue);
                bundle.putDouble("maximum", maximumValue);
                bundle.putDoubleArray("ratings", ratings);
                bundle.putString("filtroUniverso", selectedUniverse.getText().toString());
                intent.putExtras(bundle);
                startActivityForResult(intent, 101);
            }
        });

        rangeSeekBar = new RangeSeekBar<>(this);
        rangeSeekBar.setTextAboveThumbsColor(getResources().getColor(R.color.color_black));
        // Add to layout
        LinearLayout layout = (LinearLayout) findViewById(R.id.seek);
        layout.addView(rangeSeekBar);

        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Double>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Double minValue, Double maxValue) {

                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
                nf.setMinimumFractionDigits(2);
                String numeroformateado = nf.format(minValue);
                String numero = String.valueOf(numeroformateado);

                NumberFormat nf2 = NumberFormat.getCurrencyInstance(Locale.US);
                nf2.setMinimumFractionDigits(2);
                String numeroformateado2 = nf.format(maxValue);
                String numero2 = String.valueOf(numeroformateado2);

                minimumValue = minValue;
                maximumValue = maxValue;

            }
        });

        //region checkboxes
        stars_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    ratings[0] = 1.0;
                } else {
                    ratings[0] = 0.0;
                }
            }
        });

        stars_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    ratings[1] = 2.0;
                } else {
                    ratings[1] = 0.0;
                }
            }
        });

        stars_3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    ratings[2] = 3.0;
                } else {
                    ratings[2] = 0.0;
                }
            }
        });

        stars_4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    ratings[3] = 4.0;
                } else {
                    ratings[3] = 0.0;
                }
            }
        });

        stars_5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    ratings[4] = 5.0;
                } else {
                    ratings[4] = 0.0;
                }
            }
        });
        //endregion

        if (Funciones.isNetAvailable(FilterActivity.this)) {
            getGames();
        } else {
            getGamesOffline();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getGames() {
        String url = FilterActivity.this.getString(R.string.url)+"classes/Product";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        games = parseJson(response);
                        Collections.sort(games, new Comparator<Game>() {
                            @Override
                            public int compare(Game game, Game t1) {
                                return Double.compare(game.getPrice(),t1.getPrice());
                            }
                        });

                        Game all = new Game("1", "All", "All", "",
                                "", 0.0, "", "", 0.0, 0.0,
                                false, new Date(), new Date());
                        universes.add(0, all);
                        addUniverses();

                        if (games.size() > 0) {
                            int size = games.size();
                            minimumValue = games.get(0).getPrice();
                            maximumValue = games.get(size-1).getPrice();

                            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
                            nf.setMinimumFractionDigits(2);
                            String numeroformateado = nf.format(games.get(0).getPrice());
                            String numero = String.valueOf(numeroformateado);

                            NumberFormat nf2 = NumberFormat.getCurrencyInstance(Locale.US);
                            nf2.setMinimumFractionDigits(2);
                            String numeroformateado2 = nf.format(games.get(size-1).getPrice());
                            String numero2 = String.valueOf(numeroformateado2);

                            // Set the range
                            rangeSeekBar.setRangeValues(games.get(0).getPrice(), games.get(size-1).getPrice());
                            rangeSeekBar.setSelectedMinValue(games.get(0).getPrice());
                            rangeSeekBar.setSelectedMaxValue(games.get(size-1).getPrice());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error_getUniverses", error.getMessage());
                    }
                })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("X-Parse-Application-Id", FilterActivity.this.getString(R.string.parse_app_id));
                headers.put("X-Parse-REST-API-Key", FilterActivity.this.getString(R.string.rest_api_key));
                return headers;
            }
        };
        request.setTag("Filter");
        MyVolleySingleton.getInstance(FilterActivity.this.getApplicationContext()).addToRequestQueue(request);
    }

    public ArrayList<Game> parseJson(JSONObject jsonObject) {
        ArrayList<Game> games = new ArrayList<>();
        universes = new ArrayList<>();
        JSONArray jsonArray = null;

        try {
            jsonArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);

                Date created = null, updated = null;
                String createdDate = object.getString("createdAt");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                try {
                    created = format.parse(createdDate);
                    System.out.println(created);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String updatedDate = object.getString("updatedAt");
                try {
                    updated = format.parse(updatedDate);
                    System.out.println(updated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Double downloads = Double.valueOf(object.getString("downloads"));
                Double price = Double.valueOf(object.getString("price").replace(",", "."));

                Game movie = new Game(
                        object.getString("objectId"),
                        object.getString("name"),
                        object.getString("universe"),
                        object.getString("imageURL"),
                        object.getString("kind"),
                        downloads,
                        object.getString("description"),
                        object.getString("SKU"),
                        price,
                        object.getDouble("rating"),
                        object.getBoolean("popular"),
                        created,
                        updated
                );

                games.add(movie);

                boolean added = false;
                for (int j = 0; j < universes.size(); j++) {
                    if(universes.get(j).getUniverse().equals(movie.getUniverse())){
                        added = true;
                        break;
                    }
                }

                if (!added) {
                    universes.add(movie);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return games;
    }

    public void addUniverses (){

        for (int i = 0; i < universes.size(); i++) {
            RadioButton radioButton = new RadioButton(FilterActivity.this);
            radioButton.setText(universes.get(i).getUniverse());
            radioButton.setId(i+100);
            if (i == 0) {
                radioButton.setChecked(true);
            }
            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(5,5,5,5);
            radioButton.setLayoutParams(params);

            radioGroupUniverses.addView(radioButton);

        }
    }

    public void getGamesOffline(){
        games = parseJsonOffline();
        Collections.sort(games, new Comparator<Game>() {
            @Override
            public int compare(Game game, Game t1) {
                return Double.compare(game.getPrice(),t1.getPrice());
            }
        });

        addUniverses();

        if (games.size() > 0) {
            int size = games.size();
            minimumValue = games.get(0).getPrice();
            maximumValue = games.get(size-1).getPrice();

            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
            nf.setMinimumFractionDigits(2);
            String numeroformateado = nf.format(games.get(0).getPrice());
            String numero = String.valueOf(numeroformateado);

            NumberFormat nf2 = NumberFormat.getCurrencyInstance(Locale.US);
            nf2.setMinimumFractionDigits(2);
            String numeroformateado2 = nf.format(games.get(size-1).getPrice());
            String numero2 = String.valueOf(numeroformateado2);

            // Set the range
            rangeSeekBar.setRangeValues(games.get(0).getPrice(), games.get(size-1).getPrice());
            rangeSeekBar.setSelectedMinValue(games.get(0).getPrice());
            rangeSeekBar.setSelectedMaxValue(games.get(size-1).getPrice());
        }
    }

    public ArrayList<Game> parseJsonOffline(){
        ArrayList<Game> game;
        universes = new ArrayList<>();

        Realm realm = Realm.getDefaultInstance();
        controller_consultas consultas = new controller_consultas(realm);
        ArrayList<Game> lista = consultas.getJuegos();
        game = consultas.getJuegos();
        realm.close();

        Game all = new Game("1", "All", "All", "", "", 0.0, "", "", 0.0, 0.0,
                false, new Date(), new Date());
        universes.add(0, all);

        for (int i = 0; i < lista.size(); i++) {
            Boolean added = false;

            Game item = new Game(lista.get(i).getObjectId(), lista.get(i).getName(), lista.get(i).getUniverse(), lista.get(i).getImageUrl(), lista.get(i).getKind()
                    , lista.get(i).getDownloads(), lista.get(i).getDescription(), lista.get(i).getSku(), lista.get(i).getPrice(), lista.get(i).getRating(),
                    lista.get(i).getPopular(), lista.get(i).getCreatedAt(), lista.get(i).getUpdatedAt());

            for (int j = 0; j < universes.size(); j++) {
                if(universes.get(j).getUniverse().equals(item.getUniverse())){
                    added = true;
                    break;
                }
            }

            if (!added){
                universes.add(item);
            }
        }

        return game;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101){
            if (resultCode == FilterActivity.RESULT_OK){
                boolean result = data.getBooleanExtra("clear", false);
                if (result){
                    radioGroup.check(R.id.radio_popular);
                    int size = games.size();
                    rangeSeekBar.setSelectedMinValue(games.get(0).getPrice());
                    rangeSeekBar.setSelectedMaxValue(games.get(size-1).getPrice());
                    minimumValue = games.get(0).getPrice();
                    maximumValue = games.get(size-1).getPrice();
                    stars_1.setChecked(false);
                    stars_2.setChecked(false);
                    stars_3.setChecked(false);
                    stars_4.setChecked(false);
                    stars_5.setChecked(false);
                    for (int i = 0; i < 5; i++) {
                        ratings[i] = 0.0;
                    }

                    radioGroupUniverses.removeAllViews();
                    addUniverses();
                }
            }
        }
    }
}
