package test.koombea.game.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import test.koombea.game.R;
import test.koombea.game.helpers.Funciones;
import test.koombea.game.helpers.MyVolleySingleton;
import test.koombea.game.helpers.controller_consultas;
import test.koombea.game.models.Game;

public class newGamesAdapter extends RecyclerView.Adapter<newGamesAdapter.ViewHolder> {

    private ArrayList<Game> juegos;
    private ViewHolder.onGameClicklistener clicklistener;
    private static final String TAG = "Main_2";
    private Activity _activty;
    private TextView txtNew;

    public newGamesAdapter(ViewHolder.onGameClicklistener clicklistener, Activity _activty, TextView txtNew) {
        this.clicklistener = clicklistener;
        this._activty = _activty;
        this.txtNew = txtNew;
        if (Funciones.isNetAvailable(_activty.getApplicationContext())) {
            getNewGames();
        } else {
            Realm realm = Realm.getDefaultInstance();
            controller_consultas consultas = new controller_consultas(realm);
            ArrayList<Game> lista = consultas.getJuegos();
            realm.close();
            juegos = parseLista(lista);

            if (juegos.size() > 0) {
                txtNew.setText("New ("+(juegos.size())+")");
            }
            notifyDataSetChanged();
        }
    }

    public newGamesAdapter(ViewHolder.onGameClicklistener clicklistener, Activity _activty, TextView txtNew, String universe) {
        this.clicklistener = clicklistener;
        this._activty = _activty;
        this.txtNew = txtNew;
        if (Funciones.isNetAvailable(_activty.getApplicationContext())) {
            getNewGames(universe);
        } else {
            Realm realm = Realm.getDefaultInstance();
            controller_consultas consultas = new controller_consultas(realm);
            ArrayList<Game> lista = consultas.getJuegos();
            realm.close();
            juegos = parseLista(lista, universe);

            if (juegos.size() > 0) {
                txtNew.setText("New ("+(juegos.size())+")");
            }
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public newGamesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_juego, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Game mGame = juegos.get(position);
        holder.nombre_juego.setText(mGame.getName());
        RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
        Glide.with(_activty.getApplicationContext())
                .load(mGame.getImageUrl())
                .apply(requestOptions)
                .into(holder.imagen_juego);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicklistener.onNewGamesClick(mGame);
            }
        });
    }

    @Override
    public int getItemCount() {
        return juegos!= null ? juegos.size(): 0;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView nombre_juego;
        public ImageView imagen_juego;

        public ViewHolder(View itemView) {
            super(itemView);
            nombre_juego = (TextView) itemView.findViewById(R.id.nombre_juego);
            imagen_juego = (ImageView) itemView.findViewById(R.id.imagen_juego);
        }

        public interface onGameClicklistener {
            void onNewGamesClick(Game mGame);
        }
    }

    private void getNewGames() {
        String url = _activty.getString(R.string.url)+"classes/Product";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        juegos = parseJson(response);
                        if (juegos.size() > 0) {
                            txtNew.setText("New ("+(juegos.size())+")");
                        }
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error_getUniverses", error.getMessage());
                    }
                })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("X-Parse-Application-Id", _activty.getString(R.string.parse_app_id));
                headers.put("X-Parse-REST-API-Key", _activty.getString(R.string.rest_api_key));
                return headers;
            }
        };
        request.setTag(TAG);
        MyVolleySingleton.getInstance(_activty.getApplicationContext()).addToRequestQueue(request);
    }

    private void getNewGames(final String universe) {
        String url = _activty.getString(R.string.url)+"classes/Product";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        juegos = parseJson(response, universe);
                        if (juegos.size() > 0) {
                            txtNew.setText("New ("+(juegos.size())+")");
                        }
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error_getUniverses", error.getMessage());
                    }
                })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("X-Parse-Application-Id", _activty.getString(R.string.parse_app_id));
                headers.put("X-Parse-REST-API-Key", _activty.getString(R.string.rest_api_key));
                return headers;
            }
        };
        request.setTag(TAG);
        MyVolleySingleton.getInstance(_activty.getApplicationContext()).addToRequestQueue(request);
    }

    private ArrayList<Game> parseJson(JSONObject jsonObject) {
        ArrayList<Game> games = new ArrayList<>();
        ArrayList<Game> newGames = new ArrayList<>();
        JSONArray jsonArray = null;

        try {
            jsonArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);

                Date created = null, updated = null;
                String createdDate = object.getString("createdAt");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                try {
                    created = format.parse(createdDate);
                    System.out.println(created);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String updatedDate = object.getString("updatedAt");
                try {
                    updated = format.parse(updatedDate);
                    System.out.println(updated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Double downloads = Double.valueOf(object.getString("downloads"));
                Double price = Double.valueOf(object.getString("price").replace(",", "."));

                Game movie = new Game(
                        object.getString("objectId"),
                        object.getString("name"),
                        object.getString("universe"),
                        object.getString("imageURL"),
                        object.getString("kind"),
                        downloads,
                        object.getString("description"),
                        object.getString("SKU"),
                        price,
                        object.getDouble("rating"),
                        object.getBoolean("popular"),
                        created,
                        updated
                );

                games.add(movie);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Collections.sort(games, new Comparator<Game>() {
            @Override
            public int compare(Game game, Game t1) {
                if(game.getCreatedAt() == null || t1.getCreatedAt() == null)
                    return 0;
                return game.getCreatedAt().compareTo(t1.getCreatedAt());
            }
        });

        if(games.size() >= 5){
            for (int i = 0; i < 5; i++) {
                newGames.add(games.get(i));
            }
        } else {
            newGames.addAll(games);
        }

        return newGames;
    }

    private ArrayList<Game> parseJson(JSONObject jsonObject, String universe) {
        ArrayList<Game> games = new ArrayList<>();
        ArrayList<Game> newGames = new ArrayList<>();
        JSONArray jsonArray = null;

        try {
            jsonArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);

                Date created = null, updated = null;
                String createdDate = object.getString("createdAt");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                try {
                    created = format.parse(createdDate);
                    System.out.println(created);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String updatedDate = object.getString("updatedAt");
                try {
                    updated = format.parse(updatedDate);
                    System.out.println(updated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Double downloads = Double.valueOf(object.getString("downloads"));
                Double price = Double.valueOf(object.getString("price").replace(",", "."));

                if (object.getString("universe").equals(universe)) {
                    Game movie = new Game(
                            object.getString("objectId"),
                            object.getString("name"),
                            object.getString("universe"),
                            object.getString("imageURL"),
                            object.getString("kind"),
                            downloads,
                            object.getString("description"),
                            object.getString("SKU"),
                            price,
                            object.getDouble("rating"),
                            object.getBoolean("popular"),
                            created,
                            updated
                    );

                    games.add(movie);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Collections.sort(games, new Comparator<Game>() {
            @Override
            public int compare(Game game, Game t1) {
                if(game.getCreatedAt() == null || t1.getCreatedAt() == null)
                    return 0;
                return game.getCreatedAt().compareTo(t1.getCreatedAt());
            }
        });

        if(games.size() >= 5){
            for (int i = 0; i < 5; i++) {
                newGames.add(games.get(i));
            }
        } else {
            newGames.addAll(games);
        }

        return newGames;
    }

    private ArrayList<Game> parseLista (ArrayList<Game> lista){
        ArrayList<Game> newGames = new ArrayList<>();

        Collections.sort(lista, new Comparator<Game>() {
            @Override
            public int compare(Game game, Game t1) {
                if(game.getCreatedAt() == null || t1.getCreatedAt() == null)
                    return 0;
                return game.getCreatedAt().compareTo(t1.getCreatedAt());
            }
        });

        if(lista.size() >= 5){
            for (int i = 0; i < 5; i++) {
                newGames.add(lista.get(i));
            }
        } else {
            newGames.addAll(lista);
        }

        return newGames;

    }

    private ArrayList<Game> parseLista(ArrayList<Game> lista, String universe){
        ArrayList<Game> newGames = new ArrayList<>();
        ArrayList<Game> filteredGames = new ArrayList<>();

        for (int i = 0; i < lista.size(); i++) {
            if(lista.get(i).getUniverse().equals(universe)){
                filteredGames.add(lista.get(i));
            }
        }

        Collections.sort(filteredGames, new Comparator<Game>() {
            @Override
            public int compare(Game game, Game t1) {
                if(game.getCreatedAt() == null || t1.getCreatedAt() == null)
                    return 0;
                return game.getCreatedAt().compareTo(t1.getCreatedAt());
            }
        });

        if(filteredGames.size() >= 5){
            for (int i = 0; i < 5; i++) {
                newGames.add(filteredGames.get(i));
            }
        } else {
            newGames.addAll(filteredGames);
        }

        return newGames;
    }
}
