package test.koombea.game.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import test.koombea.game.R;
import test.koombea.game.helpers.Funciones;
import test.koombea.game.helpers.MyVolleySingleton;
import test.koombea.game.helpers.controller_consultas;
import test.koombea.game.models.Game;

/**
 * Created by USER on 24/06/2019.
 */

public class universosAdapter extends RecyclerView.Adapter<universosAdapter.ViewHolder> {

    private RequestQueue queue;
    private ArrayList<Game> universos;
    private ViewHolder.onUniversosClicklistener clicklistener;
    private static final String TAG = "Main";
    private Activity _activty;
    private int selectedPos = RecyclerView.NO_POSITION;

    public universosAdapter(Activity activity, ViewHolder.onUniversosClicklistener clicklistener){
        this._activty = activity;
        this.clicklistener = clicklistener;
        queue = MyVolleySingleton.getInstance(activity.getApplicationContext()).getRequestQueue();
        if (Funciones.isNetAvailable(activity.getApplicationContext())) {
            getUniverses();
        } else {
            Realm realm = Realm.getDefaultInstance();
            controller_consultas consultas = new controller_consultas(realm);
            ArrayList<Game> lista = consultas.getJuegos();
            realm.close();
            universos = new ArrayList<>();

            Game all = new Game("1", "All", "All", "", "", 0.0, "", "", 0.0, 0.0,
                    false, new Date(), new Date());
            universos.add(0, all);

            for (int i = 0; i < lista.size(); i++) {
                Boolean added = false;

                Game item = new Game(lista.get(i).getObjectId(), lista.get(i).getName(), lista.get(i).getUniverse(), lista.get(i).getImageUrl(), lista.get(i).getKind()
                        , lista.get(i).getDownloads(), lista.get(i).getDescription(), lista.get(i).getSku(), lista.get(i).getPrice(), lista.get(i).getRating(),
                        lista.get(i).getPopular(), lista.get(i).getCreatedAt(), lista.get(i).getUpdatedAt());

                for (int j = 0; j < universos.size(); j++) {
                    if(universos.get(j).getUniverse().equals(item.getUniverse())){
                        added = true;
                        break;
                    }
                }

                if (!added){
                    universos.add(item);
                }
            }

            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public universosAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_universo, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull universosAdapter.ViewHolder holder, int position) {
        //holder.itemView.setBackgroundColor(selectedPos == position ? _activty.getResources().getColor(R.color.text_color_pink): Color.TRANSPARENT);

        final Game mGame = universos.get(position);
        holder.nombre_universo.setText(mGame.getUniverse());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicklistener.onUniversosClick(mGame);
            }
        });
    }

    @Override
    public int getItemCount() {
        return universos!= null ? universos.size(): 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView nombre_universo;

        public ViewHolder(View itemView) {
            super(itemView);
            nombre_universo = (TextView) itemView.findViewById(R.id.universe_name);
        }

        public interface onUniversosClicklistener {
            void onUniversosClick(Game mGame);
        }
    }

    private void getUniverses() {
        String url = _activty.getString(R.string.url)+"classes/Product";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        universos = parseJson(response);
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error_getUniverses", error.getMessage());
                    }
                })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("X-Parse-Application-Id", _activty.getString(R.string.parse_app_id));
                headers.put("X-Parse-REST-API-Key", _activty.getString(R.string.rest_api_key));
                return headers;
            }
        };
        request.setTag(TAG);
        MyVolleySingleton.getInstance(_activty.getApplicationContext()).addToRequestQueue(request);
    }

    private ArrayList<Game> parseJson(JSONObject jsonObject) {
        ArrayList<Game> games = new ArrayList<>();
        JSONArray jsonArray = null;
        Game all = new Game(
                "1",
                "All",
                "All",
                "",
                "",
                0.0,
                "",
                "",
                0.0,
                0.0,
                false,
                new Date(),
                new Date()
        );
        games.add(all);

        try {
            jsonArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);

                Date created = null, updated = null;
                String createdDate = object.getString("createdAt");
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                try {
                    created = format.parse(createdDate);
                    System.out.println(created);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String updatedDate = object.getString("updatedAt");
                try {
                    updated = format.parse(updatedDate);
                    System.out.println(updated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Double downloads = Double.valueOf(object.getString("downloads"));
                Double price = Double.valueOf(object.getString("price").replace(",", "."));

                Game movie = new Game(
                        object.getString("objectId"),
                        object.getString("name"),
                        object.getString("universe"),
                        object.getString("imageURL"),
                        object.getString("kind"),
                        downloads,
                        object.getString("description"),
                        object.getString("SKU"),
                        price,
                        object.getDouble("rating"),
                        object.getBoolean("popular"),
                        created,
                        updated
                );

                boolean added = false;
                for (int j = 0; j < games.size(); j++) {
                    if(games.get(j).getUniverse().equals(movie.getUniverse())){
                        added = true;
                        break;
                    }
                }

                if (!added) {
                    games.add(movie);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return games;
    }
}
