package test.koombea.game.helpers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by USER on 24/06/2019.
 */

public final class MyVolleySingleton {

    private static MyVolleySingleton singleton;
    private RequestQueue requestQueue;
    private static Context context;

    private MyVolleySingleton(Context context){
        singleton.context = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized MyVolleySingleton getInstance(Context context) {
        if (singleton == null) {
            singleton = new MyVolleySingleton(context);
        }
        return singleton;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    public  void addToRequestQueue(Request req) {
        getRequestQueue().add(req);
    }

}
