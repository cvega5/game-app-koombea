package test.koombea.game.helpers;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import test.koombea.game.models.Game;

public class controller_consultas {

    private Realm realm;

    public controller_consultas(Realm realm) {
        this.realm = realm;
    }

    public void guardarJuegos(final ArrayList<Game> items){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(items);
            }
        });
    }

    public void eliminarJuegos(){
        final RealmResults<Game> results = realm.where(Game.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteAllFromRealm();
            }
        });
    }

    public ArrayList<Game> getJuegos(){
        RealmResults<Game> results = realm.where(Game.class).findAll();
        return new ArrayList<>(realm.copyFromRealm(results));
    }

}
