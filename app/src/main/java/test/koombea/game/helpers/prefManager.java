package test.koombea.game.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by USER on 24/06/2019.
 */

public class prefManager {
    SharedPreferences credenciales_usuario;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "androidhive-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public prefManager(Context context) {
        this._context = context;
        credenciales_usuario = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        credenciales_usuario.edit().putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime).apply();
    }

    public boolean isFirstTimeLaunch() {
        return credenciales_usuario.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
}
